from setuptools import setup
setup(
    name='vimeo',
    version='1.0.1',
    py_modules=['vimeo'],
    install_requires = ['markdown>=2.5'],
)