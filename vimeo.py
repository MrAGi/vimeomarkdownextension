import markdown
from markdown.preprocessors import Preprocessor
from markdown.extensions import Extension
import re

class VimeoPreprocessor(Preprocessor):
    def run(self,lines):
        new_lines = []
        vimeo = "^!\[vimeo\]\(\d+\)$"
        for line in lines:
            if re.match(vimeo, line):
                components = line.split("(")
                video = components[1][:-1]
                html = """<div class="flex-video widescreen vimeo"><iframe src="//player.vimeo.com/video/{0}?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>""".format(video)
                new_lines.append(html)
                new_lines.append("</iframe></div>")
            else:
                new_lines.append(line)
        return new_lines

class VimeoExtension(Extension):
    def extendMarkdown(self,md,md_globals):
        md.preprocessors.add('vimeo',VimeoPreprocessor(md),"<reference")

def makeExtension(**kwargs):
    return VimeoExtension(**kwargs)
